import 'dart:core';
//import "dart:io";
main() {
  String s1 = "hello";
  print(s1.indexOf("ll"));

  var simpleList = [1,2,3];
  print(simpleList);
  var listOfVegetables = ['potato', 'carrot', 'cucumber'];
  print(listOfVegetables[1]);
  print(listOfVegetables.length);
  listOfVegetables.add("cabbage");
  print(listOfVegetables);
  listOfVegetables.addAll(['broccoli','zucchini']);
  print(listOfVegetables);
  listOfVegetables.removeAt(0);
  print(listOfVegetables);
  print(listOfVegetables.indexOf('carrot'));
  listOfVegetables.clear();
  print("");

  var numbers = [1,2,3,4,5,6,7,8,9,10];
  print("The squares of those numbers are: ");
  print(numbers.map((numberInList) => numberInList*numberInList));
  print("");

  var simpleSet = <num>{1,2,2,3,3,3}; //The <num> is optional
  //Set<num> simpleSetCPPStyle = {1,2,2,3,3,3}; Or you can go full C++ mode and use this
  print(simpleSet);
  print("");

  Set<dynamic> setOfFruit = {};
  setOfFruit.add('apples');
  setOfFruit.add('bananas');
  setOfFruit.add('oranges');
  print(setOfFruit);
  setOfFruit.addAll(simpleSet);
  setOfFruit.addAll(listOfVegetables); //Lists don't work, but they don't give an error either
  print(setOfFruit);
  setOfFruit.remove(1);
  print(setOfFruit);

  var containBools = [];
  containBools.add(setOfFruit.contains('apples'));
  containBools.add(setOfFruit.contains('grapes'));
  containBools.add(setOfFruit.containsAll(setOfFruit)); //containsAll can be used as a subset check, and this one is true because every set is a subset of itself
  print(containBools);
  var setOfFruits = {'apples', 'oranges', 'watermelon', 'grapes'};
  var setOfMoreFruits = {'oranges', 'kiwi', 'bananas'};
  var intersectionSet = setOfFruits.intersection(setOfMoreFruits);
  var unionSet = setOfFruits.union(setOfMoreFruits);
  print(intersectionSet);
  print(unionSet);
  print("");

  //Maps are similar to dictionaries in Python
  var capitals = {
    'United States' : 'Washington D.C.',
    'England' : 'London',
    'China' : 'Beijing',
    'Germany' : 'Berlin',
    'Nigeria' : 'Abuja',
    'Egypt' : 'Cairo',
    'New Zealand' : 'Wellington'
  };
  print(capitals);
  var emptyMap = Map<num, String>(); //You can specify data types, again this is optional
  print(emptyMap);
  emptyMap[1] = 'one';
  emptyMap[2] = 'two';
  emptyMap[3] = 'three';
  print(emptyMap);
  print(emptyMap.length);
  print(capitals["China"]);
  print(capitals["Turkey"]); //Doesn't give an error, just returns null
  print(capitals.containsKey("United States"));
  var countries = capitals.keys; var capitalsOfThoseCountries = capitals.values; //You can split a map's keys (left) and values (right)
  print(countries); print(capitalsOfThoseCountries);
  print(capitals);
  capitals.remove("England");
}