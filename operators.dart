main() {
  var operand1 = 10;
  var operand2 = 7;

  print(operand1 + operand2);
  print(operand1 - operand2);
  print(- operand1);
  print(operand1 * operand2);
  print(operand1 / operand2);
  print(operand1 ~/ operand2);
  print(operand1 % operand2);
  print("");

  operand1++; operand2--;
  print(operand1);
  print(operand2);
  print("");

  print(operand1 > operand2);
  print(operand1 < operand2);
  print(operand1 >= operand2);
  print(operand1 <= operand2);
  print(operand1 == operand2);
  print(operand1 != operand2);
  print("");

  var character1 = 'a';
  var character2 = 'b';
  print(character1 == character2);
  //print(character1<character2); Doesn't work
  print("");

  print(operand1 is int);
  print(operand2 is! int);
  print("");

  operand1 += operand2;
  print(operand1);
  operand2 -= operand1;
  print(operand2);
  print("");

  bool p = true, q = false;
  print(p&&q);
  print(p||q);
  print(!p);
  print("");

  var A = 12;
  var B = 5;
  print(~A); // A complement
  print(~B); // B complement
  print(A & B); // A AND B
  print(A | B); // A OR B
  print(A ^ B); // A XOR B
  print(B << 2); // B Shift Left 2
  print(A >> 2); // A Shift Right 2
}