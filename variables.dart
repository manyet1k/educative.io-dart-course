main(){
  int x = 5;
  print(x);

  //int y;
  //print(y); Throws an error

  num y = 3; print(y);
  y = 3.1; print(y);

  String s = "This is a string";
  print(s);
  print("The content of the string is $s");
  print("5+3=${5+3}");

  bool b1 = true;
  print(b1);

  var bookTitle = "Lord of the Rings: The Fellowship of the Ring";
  print(bookTitle.runtimeType);

  dynamic a = 10;
  a = true;
  a = "asdf";

  final String finalvar = "asdf"; //can't be changed later
  const int b = 7; //same

}