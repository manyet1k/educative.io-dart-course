import "dart:io";

num subtract(num x, num y){
  return x-y;
}
num sum(num x, num y) => x+y;

int fib(int x){
  if(x==0 || x==1)
    return 1;
  else
    return fib(x-1)+fib(x-2);
}

void forAll(Function f, List<dynamic> x){
  for(dynamic i in x) {
    stdout.write(f(i));
    stdout.write(" ");
  }
}

main(){
    print(sum(1,2));
    print(subtract(1,2));
    print("");

    print(fib(5));
    forAll(fib, [0,1,2,3,4,5,6,7,8,9]);
    print("");
    [0,1,2,3,4,5,6,7,8,9].forEach((element) {stdout.write(element); stdout.write(" ");});

}