main() {
  var testList = [2, 4, 8, 16, 32];
  print(testList);
  if(testList.contains(4))
    print("The number 4 is in the list");
  else
    print("The number 4 is not in the list");
  if(testList.contains(5))
    print("The number 5 is in the list");
  else
    print("The number 5 is not in the list");

  // condition ? ifTrue : ifFalse
  var ternaryIfElse = testList.contains(4) ? 4 : 10;
  print(ternaryIfElse);

  for(int i=0;i<100;i++)
    print(i);

  var colorList = ['blue', 'yellow', 'green', 'red'];
  for(var i in colorList){
    print(i);
  }

  var count = 1;
  while (count <= 10) {
    print(count);
    count += 1;
  }

  switch(count){
    case 1:
      print("one");
      break;
    case 2:
      print("two");
      break;
    default:
      print("Unknown number");
      break;
  }
}