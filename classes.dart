class Student{
  String name="Error: Name not defined";
  String year="Error: Year not defined";
  double GPA=-1;
  int SATScore=-1;

  /*Student(String nameC, String yearC, double GPAC, int SATScoreC){
    this.name = nameC;
    this.year = yearC;
    this.GPA = GPAC;
    this.SATScore = SATScoreC;
  }*/ //The long and boring way to write a constructor
  Student(this.name, this.year, this.GPA, this.SATScore); //The faster way
  Student.newToHS(){
    year = "Freshman";
  }

  String get getName => name;
  set setName(String nameS){name=nameS;}
  printGPA() => print("The GPA of $name is $GPA");
}

class InternationalStudent extends Student{
  int TOEFLScore = -1;

  InternationalStudent(String name, String year, double GPA, int SATScore, int TOEFLScoreC) : TOEFLScore=TOEFLScoreC, super(name, year, GPA, SATScore);
}

main(){
  var student1 = Student("name","year",0,0);
  Student student2 = Student("name","year",0,0);

  student1.name = "Alice"; student2.name = "Bob";
  student1.GPA = 3.7; student2.year = "Junior";
  student1.printGPA();
}